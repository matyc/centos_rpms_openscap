From 89f99834ba183284a7d75835932a0c0ea4eb9007 Mon Sep 17 00:00:00 2001
From: Evgeny Kolesnikov <ekolesni@redhat.com>
Date: Mon, 17 May 2021 08:40:17 +0200
Subject: [PATCH] oval/yamlfilecontent: Add 'null' values handling

For now null values would be represented as string '(null)' as
record's field could not be attributed as nil="true" yet.
---
 .../independent/yamlfilecontent_probe.c       |  9 ++++
 .../test_probes_yamlfilecontent_types.sh      |  5 ++
 .../test_probes_yamlfilecontent_types.xml     | 52 +++++++++++++++++++
 tests/probes/yamlfilecontent/types.yaml       |  4 ++
 4 files changed, 70 insertions(+)

diff --git a/src/OVAL/probes/independent/yamlfilecontent_probe.c b/src/OVAL/probes/independent/yamlfilecontent_probe.c
index 62a8f4ff29..2d0cac6991 100644
--- a/src/OVAL/probes/independent/yamlfilecontent_probe.c
+++ b/src/OVAL/probes/independent/yamlfilecontent_probe.c
@@ -41,6 +41,7 @@
 #define OSCAP_YAML_BOOL_TAG "tag:yaml.org,2002:bool"
 #define OSCAP_YAML_FLOAT_TAG "tag:yaml.org,2002:float"
 #define OSCAP_YAML_INT_TAG "tag:yaml.org,2002:int"
+#define OSCAP_YAML_NULL_TAG "tag:yaml.org,2002:null"
 
 #define OVECCOUNT 30 /* should be a multiple of 3 */
 
@@ -135,6 +136,14 @@ static SEXP_t *yaml_scalar_event_to_sexp(yaml_event_t *event)
 			return NULL;
 		}
 	}
+	if (question || !strcmp(tag, OSCAP_YAML_NULL_TAG)) {
+		if (match_regex("^(null|Null|NULL|~|)$", value)) {
+			// TODO: Return real NULL when record's field will support nil="true"
+			return SEXP_string_new("(null)", strlen("(null)"));
+		} else if (!question) {
+			return NULL;
+		}
+	}
 
 	return SEXP_string_new(value, strlen(value));
 }
diff --git a/tests/probes/yamlfilecontent/test_probes_yamlfilecontent_types.sh b/tests/probes/yamlfilecontent/test_probes_yamlfilecontent_types.sh
index 4f110f6eb7..e445771d03 100755
--- a/tests/probes/yamlfilecontent/test_probes_yamlfilecontent_types.sh
+++ b/tests/probes/yamlfilecontent/test_probes_yamlfilecontent_types.sh
@@ -60,6 +60,11 @@ function test_probes_yamlfilecontent_types {
     assert_exists 1 $sd'/ind-sys:yamlfilecontent_item/ind-sys:value/field[@name="#" and @datatype!="boolean" and text()="true"]'
     # string_number
     assert_exists 1 $sd'/ind-sys:yamlfilecontent_item/ind-sys:value/field[@name="#" and @datatype!="int" and text()="81"]'
+    # string_null
+    assert_exists 1 $sd'/ind-sys:yamlfilecontent_item/ind-sys:value/field[@name="#" and text()="null"]'
+
+    # null_1_2_3
+    assert_exists 3 $sd'/ind-sys:yamlfilecontent_item/ind-sys:value/field[@name="#" and text()="(null)"]'
 
     # bool_error_cast, int_error_cast, float_error_cast
     co='/oval_results/results/system/oval_system_characteristics/collected_objects'
diff --git a/tests/probes/yamlfilecontent/test_probes_yamlfilecontent_types.xml b/tests/probes/yamlfilecontent/test_probes_yamlfilecontent_types.xml
index adf96571b8..503ec2d4a4 100644
--- a/tests/probes/yamlfilecontent/test_probes_yamlfilecontent_types.xml
+++ b/tests/probes/yamlfilecontent/test_probes_yamlfilecontent_types.xml
@@ -262,6 +262,19 @@
       </criteria>
     </definition>
 
+    <definition class="compliance" version="1" id="oval:0:def:26">
+      <metadata>
+        <title></title>
+        <description></description>
+      </metadata>
+      <criteria operator="AND">
+        <criterion comment="comment" test_ref="oval:0:tst:26"/>
+        <criterion comment="comment" test_ref="oval:0:tst:27"/>
+        <criterion comment="comment" test_ref="oval:0:tst:28"/>
+        <criterion comment="comment" test_ref="oval:0:tst:29"/>
+      </criteria>
+    </definition>
+
   </definitions>
 
   <tests>
@@ -364,6 +377,21 @@
       <ind-def:object object_ref="oval:0:obj:25"/>
     </ind-def:yamlfilecontent_test>
 
+    <ind-def:yamlfilecontent_test version="1" id="oval:0:tst:26" check="all" comment="true">
+      <ind-def:object object_ref="oval:0:obj:26"/>
+    </ind-def:yamlfilecontent_test>
+
+    <ind-def:yamlfilecontent_test version="1" id="oval:0:tst:27" check="all" comment="true">
+      <ind-def:object object_ref="oval:0:obj:27"/>
+    </ind-def:yamlfilecontent_test>
+
+    <ind-def:yamlfilecontent_test version="1" id="oval:0:tst:28" check="all" comment="true">
+      <ind-def:object object_ref="oval:0:obj:28"/>
+    </ind-def:yamlfilecontent_test>
+
+    <ind-def:yamlfilecontent_test version="1" id="oval:0:tst:29" check="all" comment="true">
+      <ind-def:object object_ref="oval:0:obj:29"/>
+    </ind-def:yamlfilecontent_test>
   </tests>
 
   <objects>
@@ -517,6 +545,30 @@
       <ind-def:filename>types.yaml</ind-def:filename>
       <ind-def:yamlpath>.float_error_cast</ind-def:yamlpath>
     </ind-def:yamlfilecontent_object>
+
+    <ind-def:yamlfilecontent_object version="1" id="oval:0:obj:26">
+      <ind-def:path>/tmp</ind-def:path>
+      <ind-def:filename>types.yaml</ind-def:filename>
+      <ind-def:yamlpath>.null_1</ind-def:yamlpath>
+    </ind-def:yamlfilecontent_object>
+
+    <ind-def:yamlfilecontent_object version="1" id="oval:0:obj:27">
+      <ind-def:path>/tmp</ind-def:path>
+      <ind-def:filename>types.yaml</ind-def:filename>
+      <ind-def:yamlpath>.null_2</ind-def:yamlpath>
+    </ind-def:yamlfilecontent_object>
+
+    <ind-def:yamlfilecontent_object version="1" id="oval:0:obj:28">
+      <ind-def:path>/tmp</ind-def:path>
+      <ind-def:filename>types.yaml</ind-def:filename>
+      <ind-def:yamlpath>.null_3</ind-def:yamlpath>
+    </ind-def:yamlfilecontent_object>
+
+    <ind-def:yamlfilecontent_object version="1" id="oval:0:obj:29">
+      <ind-def:path>/tmp</ind-def:path>
+      <ind-def:filename>types.yaml</ind-def:filename>
+      <ind-def:yamlpath>.string_null</ind-def:yamlpath>
+    </ind-def:yamlfilecontent_object>
   </objects>
 
 </oval_definitions>
diff --git a/tests/probes/yamlfilecontent/types.yaml b/tests/probes/yamlfilecontent/types.yaml
index f05fa3a967..fb26eab5f0 100644
--- a/tests/probes/yamlfilecontent/types.yaml
+++ b/tests/probes/yamlfilecontent/types.yaml
@@ -19,7 +19,11 @@ bool_false_cast: !!bool "false"
 int_cast: !!int "369"
 float_cast: !!float "978.65"
 string_true: "true"
+string_null: "null"
 string_number: "81"
 bool_error_cast: !!bool "falsee"
 int_error_cast: !!int "50%"
 float_error_cast: !!float "58.41$"
+null_1: null
+null_2:
+null_3: !!null "null"
